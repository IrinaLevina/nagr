class Source < ActiveRecord::Base
  belongs_to :category
  has_and_belongs_to_many :users
  has_many :articles
  attr_accessible :Url, :name, :user_ids, :category_id
end
