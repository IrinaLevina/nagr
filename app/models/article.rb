class Article < ActiveRecord::Base
  belongs_to :source
  attr_accessible :name, :text , :source_id, :Url, :data
end
