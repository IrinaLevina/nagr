class Category < ActiveRecord::Base
  has_many :sources
  attr_accessible :name, :source_ids
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :name, length:  {minimum: 2}
  before_save do
    self.name = name.capitalize
  end
end
