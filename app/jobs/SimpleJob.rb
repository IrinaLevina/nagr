class SimpleJob
  @queue = :simple

  def self.perform
    require 'rss'
    require 'open-uri'
    Source.order('id desc').each do |s|
      @rs = RSS::Parser.parse(open(s.Url).read, false)
      @rs.items.each do |i|
        @article = Article.find_by_name(i.title)
        unless @article
          @article = Article.new
          @article.name = i.title
          @article.text = i.description
           @article.Url = i.link
           @article.date = i.date
          s.articles << @article
          puts "save article. source: #{s.id}"
        else
          break
        end
      end
    end
    puts "Job is done"
  end
end