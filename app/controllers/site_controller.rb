class SiteController < ApplicationController
  Resque.enqueue(SimpleJob)
  before_filter :authenticate_user!
  def index
    str = params[:query_str] ? "%#{params[:query_str]}%" : "%%"
    if params[:source_id]
      @rss = Article.joins(:source, source: :users).where("users.id = ? and articles.source_id = ? and articles.text like ?", current_user.id, params[:source_id], str).order("data desc").page(params[:page]).per(params[:per])
    elsif params[:category_id]
      @rss = Article.joins(:source, source: :users).where("users.id = ? and sources.category_id = ? and articles.text like ?", current_user.id, params[:category_id],str).order("data desc").page(params[:page]).per(params[:per])
    else
      @rss = Article.joins(:source, source: :users).where("users.id = ? and articles.text like ?", current_user.id, str).order("data desc").page(params[:page]).per(params[:per])
    end
    @categories = Category.joins(:sources, sources: :users).where("users.id = ?", current_user.id).uniq!
    if params[:category_id]
      @sources1 = Category.find(params[:category_id]).sources
      @sources2 = current_user.sources
      @sources = @sources1 & @sources2
      @id =  params[:category_id]
    end

    respond_to do |format|
      format.js
      format.html
    end

  end


  def profile
    source = params[:source_ids]
    if source
      current_user.sources.clear
      source.keys.map{|s|
        current_user.sources << Source.find(s)
      }
    expire_fragment('my_profile')
    UserMailer.user_setting(current_user.id).deliver
    end
  end

  def search
  end
end
