class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name
      t.text :text
      t.references :source

      t.timestamps
    end
    add_index :articles, :source_id
  end
end
