class CreateSources < ActiveRecord::Migration
  def change
    create_table :sources do |t|
      t.string :name
      t.string :Url

      t.timestamps
    end
    add_index :sources, :name
  end
end
